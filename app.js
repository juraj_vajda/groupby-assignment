/* OOP */
let app = {
	_settings: {
		navCon: '.nav',
		productsCon: '.products'
	},

	/**
	 * Main settings object, this extends _settings
	 */
	settings: {},

	// initial store values
	store: {
		data: {
			products: [],
			nav: [
				{
					label: "title"
				},
				{
					label: "price"
				}
			]
		}
	},

	/**
	 * Create a `Product` Class
	 * - it should have a constructor
	 *   - which accepts a JSON object
	 *   - the object will carry {`title`,`desc`,`price`,`sale`}
	 *
	 * - it should create some private variables (using the JSON):
	 *   - `title`, `desc`, `price`, `sale`
	 *
	 * - it should have a `render` function
	 *   - which will return a DOM NODE used for displaying this product
	 *   - the product should consist of a DIV
	 *     - with a class `product-tile`
	 *     - that contains
	 *       - an image (http://via.placeholder.com/144x180)
	 *       - product title
	 *       - product price
	 *       - product description
	 *       - and a badge indicating whether the product is on sale
	 */
	renderProducts: function() {
		let that = this;
		let products = this.store.data.products;
		let parent = document.querySelector(that.settings.productsCon);
		parent.innerHTML = '';

		products.forEach(function(product) {
			parent.innerHTML += `
				<div class="item" data-onsale="${product.sale}">
					<div class="onsale"><span>on sale</span></div>
					<div class="item__bg" style="background-image:url('http://via.placeholder.com/305x305');"></div>
					<div class="container">
						<div class="row">
							<h4><b>${product.title}</b></h4>
							<p>${product.price}</p>
						</div>
						<p>${product.desc}</p>
					</div>
				</div>
			`;
		});
	},

	/**
	 * Create a `Nav` Class
	 * - it should have a constructor
	 *   - which accepts a JSON object
	 *   - the object will carry {`label`,`attr`}
	 *
	 * - it should create some private variables (using the JSON):
	 *   - label, attr
	 *
	 * - it should have a `render` function
	 *   - which will return a DOM NODE used for displaying this nav
	 *   - the nav should consist of
	 *     - a radio button
	 *     - a label to display the `label` text
	 *   - clicking the radio button should communicate to the Grid
	 *     - by sending the `attr` in a `CustomEvent`
	 *     - the Grid will sort the products on the provided `attr`ibute
	 */
	renderNav: function() {
		let that = this;
		let items = this.store.data.nav;
		let parent = document.querySelector(that.settings.navCon);
		parent.innerHTML = '';
		parent.innerHTML += `<form>`;

		items.forEach(function(item) {
			parent.innerHTML += `
				<input type="radio" id="${item.label}" name="sort" value="${item.label}">
				<label for="${item.label}">Sort by ${item.label}</label>
				<br/>
			`;
		});

		parent.innerHTML += `</form>`;
	},

	/**
		* Create a `Grid` class
		* - it should have a constructor
		*   - which accepts a JSON object
		*   - the object will carry `{ products:[], navs:[]}`
		*
		* - it should have a `renderProducts` function
		*   - which instantiates `Product`s for each item in the JSON array
		*   - and inserts the Product DOM Nodes into a DOM placeholder (with class `products`)
		*   - using the `Product`s `render` function
		*
		* - it should have a `renderNavs` function
		*   - which instantiates `Nav`s for each item in the JSON array
		*   - and inserts the Nav DOM Nodes into a DOM placeholder (with class `nav`)
		*   - using the `Nav`s `render` function
		*
		* - it should listen for communication from the Nav items
		*   - the Nav items will send an `attr`
		*   - the Grid should sort the `Product`s in ascending order
		*   - using the provided `attr` of the Product
		*/
	getProductsService: function() {
		let that = this;

		fetch('http://private-152094-gbitest.apiary-mock.com/products')
			.then(response => response.json())
			.then(data => {
				that.store.data.products = [...data];
				this.renderNav();
				this.renderProducts(this.data);
				this.events();
				return;
			});
	},

	events: function() {
		var that = this;
		var radios = document.querySelectorAll('input[type="radio"]');

		document.querySelectorAll('input[type="radio"]').forEach(function(radio) {
			radio.addEventListener('input', function(e) {

				if (e.target.value === 'price') {
					that.store.data.products.sort(function(a,b) {return (a.price > b.price) ? 1 : ((b.price > a.price) ? -1 : 0);});
					that.renderProducts();
				} else if (e.target.value === 'title') {
					that.store.data.products.sort(function(a,b) {return (a.title > b.title) ? 1 : ((b.title > a.title) ? -1 : 0);});
					that.renderProducts();
				}
			})
		});
	},

	/**
		* Create an event listener that fires when the window has loaded
		* - it should execute the init function
		*   - which should first read the JSON from remote endpoints
		*     - retrieve `products` from `http://private-152094-gbitest.apiary-mock.com/products`
		*     - retrieve `navs` from `https://private-152094-gbitest.apiary-mock.com/navs`
		*   - instantiate Grid using the `products` and `navs` from the received JSON
		*     - call the `renderNavs` and `renderProducts`
		*/
	init(settings) {
		console.log('[app] initialized..');
		// exted default settings
		settings = settings || {};
		this.settings = Object.assign({}, this._settings, settings);
		this.getProductsService();
	}
}

window.addEventListener('DOMContentLoaded', app.init());
